package com.qcm.home1stlending.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.qcm.home1stlending.R;
import com.qcm.home1stlending.databinding.FragmentSigninBinding;
import com.qcm.home1stlending.databinding.FragmentSignupBinding;

public class SignUpFragment extends Fragment {
    FragmentSignupBinding signupBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        signupBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);
        return signupBinding.getRoot();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
