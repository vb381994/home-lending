package com.qcm.home1stlending.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.qcm.home1stlending.R;
import com.qcm.home1stlending.databinding.ActivityAuthBinding;
import com.qcm.home1stlending.fragment.SignInFragment;
import com.qcm.home1stlending.fragment.SignUpFragment;

public class AuthActivity extends AppCompatActivity {
    ActivityAuthBinding authBinding;
    Fragment fragment = null;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authBinding = DataBindingUtil.setContentView(this, R.layout.activity_auth);

        set_tab_viewpager();
    }


    public void set_tab_viewpager(){
        authBinding.tabLayout.addTab(authBinding.tabLayout.newTab().setText(R.string.sign_in));
        authBinding.tabLayout.addTab(authBinding.tabLayout.newTab().setText(R.string.sign_up));
        authBinding.tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);


        fragment = new SignInFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

        authBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        // homeFragmentBinding.relativeSearch.setVisibility(View.VISIBLE);
                        fragment = new SignInFragment();
                        break;
                    case 1:
                        //  homeFragmentBinding.relativeSearch.setVisibility(View.GONE);
                        fragment = new SignUpFragment();
                        break;

                }
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frameLayout, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}