package com.qcm.home1stlending.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.qcm.home1stlending.R;
import com.qcm.home1stlending.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {
    ActivitySplashBinding splashBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);


        splashBinding.btnCustomer.setOnClickListener(v -> {
            startActivity(new Intent(this,AuthActivity.class));
        });
    }
}